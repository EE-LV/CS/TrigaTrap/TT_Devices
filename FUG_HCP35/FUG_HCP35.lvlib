﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,B!!!*Q(C=T:3R&lt;B."%)&lt;`1R3G4%G"&amp;.FFX-ULO-A,J+-?5&gt;+Z$1XS+UR(BW5*J5:"5+7&lt;*F*=_A5I,#JE22&lt;(&gt;ZMVMG*D.S$9P&gt;E\```O\(?\[Z.+[5F0V2YL(Q_7AXZ\W'_0_-@'`WO`7ZTD[X/M['$"0TCG]__XSOJ_?$^]\'_.X/`@XNTC&gt;?VK@&lt;&lt;?^7_I&lt;7F81]JD`R&lt;HNCXN[GR^NO.P4.L6OL?&lt;@Z/=&gt;K`@6LC/@V`_\@@&lt;Q\@;,HP]Y&lt;I4;\P(8]]\;TV9`]9@XH8CI&amp;T\`(9/W+"=_`;0=D?8"A0.N6-W@?`GV&amp;V&lt;R]`0]?.X?@S1`NUC.9753#))*[S]8:PIC:\IC:\IC2\IA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\IBFYKON#&amp;,H27*:E]G3B*GC2)/I/CJ%FY%J[%*_(BJR+?B#@B38A3(LIIY5FY%J[%*_&amp;BG"+?B#@B38A3(F)6ECQ6(:[%B`1+?!+?A#@A#8C95A&amp;0!""-&amp;C1/EI#BQ!Q?!J[!*_$B51&amp;0Q"0Q"$Q"$\9#HI!HY!FY!B['F&amp;7*1N.6&gt;(B))Y@(Y8&amp;Y("[(B^2S?"Q?B]@B=8C94A[0Q_.!/"-[S5'1-]DJY0RQ?"Q?&lt;H*Y("[(R_&amp;R?,$+$HF:G9[GK_DQ'$Q'D]&amp;D]"A]J*$"9`!90!;0Q5.;'4Q'D]&amp;D]"A]4#7$R_!R?!Q19V+GFZ(-''BU-A3$B[O=&amp;CO\&amp;)8%3KV`T9?$KHI!61_7[I&amp;201CK'[S[=;I&lt;ILL1KAOIOD#K,[T[)KK!KB/L*F4NK#8NALAG:M3%'"-8R)DI%S@&gt;U$`==&lt;F=;L&amp;9[0L[7L0:4*0*2/0R7"=8&amp;RK.2OLX_TIZ/@HVN4KF0J2G]VW[Z*FY2DSP^]@RIOOT[@OF848P.'WO@ES&lt;$^_HT?RLDTBPXL_=FLD[@)\8IU_PG:\WGE_PO:`XSLCXL^3]_6&lt;G;R[_3``$NV&amp;0V'Z^ZFGDHZT;Z&lt;M!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="public" Type="Folder">
		<Item Name="FUG_HCP35.constructor.vi" Type="VI" URL="../FUG_HCP35.constructor.vi"/>
		<Item Name="FUG_HCP35.destructor.vi" Type="VI" URL="../FUG_HCP35.destructor.vi"/>
		<Item Name="FUG_HCP35.emergency off.vi" Type="VI" URL="../FUG_HCP35.emergency off.vi"/>
		<Item Name="FUG_HCP35.get data to modify.vi" Type="VI" URL="../FUG_HCP35.get data to modify.vi"/>
		<Item Name="FUG_HCP35.set modified data.vi" Type="VI" URL="../FUG_HCP35.set modified data.vi"/>
		<Item Name="FUG_HCP35.set channel voltage.vi" Type="VI" URL="../FUG_HCP35.set channel voltage.vi"/>
		<Item Name="FUG_HCP35.set channel current.vi" Type="VI" URL="../FUG_HCP35.set channel current.vi"/>
		<Item Name="FUG_HCP35.set channel OnOff.vi" Type="VI" URL="../FUG_HCP35.set channel OnOff.vi"/>
		<Item Name="FUG_HCP35.set channel PosPolarity.vi" Type="VI" URL="../FUG_HCP35.set channel PosPolarity.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="FUG_HCP35.get i attribute.vi" Type="VI" URL="../FUG_HCP35.get i attribute.vi"/>
		<Item Name="FUG_HCP35.set i attribute.vi" Type="VI" URL="../FUG_HCP35.set i attribute.vi"/>
		<Item Name="FUG_HCP35.ProcCases.vi" Type="VI" URL="../FUG_HCP35.ProcCases.vi"/>
		<Item Name="FUG_HCP35.ProcPeriodic.vi" Type="VI" URL="../FUG_HCP35.ProcPeriodic.vi"/>
		<Item Name="FUG_HCP35.get channel current.vi" Type="VI" URL="../FUG_HCP35.get channel current.vi"/>
		<Item Name="FUG_HCP35.get channel voltage.vi" Type="VI" URL="../FUG_HCP35.get channel voltage.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="FUG_HCP35.ranges type.ctl" Type="VI" URL="../FUG_HCP35.ranges type.ctl"/>
		<Item Name="FUG_HCP35.GetAddress.vi" Type="VI" URL="../FUG_HCP35.GetAddress.vi"/>
		<Item Name="FUG_HCP35.GetRanges.vi" Type="VI" URL="../FUG_HCP35.GetRanges.vi"/>
		<Item Name="FUG_HCP35.i attribute.ctl" Type="VI" URL="../FUG_HCP35.i attribute.ctl"/>
		<Item Name="FUG_HCP35.i attribute.vi" Type="VI" URL="../FUG_HCP35.i attribute.vi"/>
		<Item Name="FUG_HCP35.icon.ctl" Type="VI" URL="../FUG_HCP35.icon.ctl"/>
		<Item Name="FUG_HCP35.ProcDestructor.vi" Type="VI" URL="../FUG_HCP35.ProcDestructor.vi"/>
		<Item Name="FUG_HCP35.ProcEvents.vi" Type="VI" URL="../FUG_HCP35.ProcEvents.vi"/>
	</Item>
	<Item Name="FUG_HCP35.contents.vi" Type="VI" URL="../FUG_HCP35.contents.vi"/>
</Library>
