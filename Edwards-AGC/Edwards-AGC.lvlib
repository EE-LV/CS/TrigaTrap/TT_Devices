﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,@!!!*Q(C=T:3R&lt;B."%)&lt;`1R3G4%G"&amp;-6FX-ULJ-A,O+-?5&gt;+F$1XS+UR(BW5*]1"'5+7&lt;*F*=_A5I,#JE22&lt;(&gt;ZMVMO,$&lt;E!Q[^GT`X^X^LO^^5EF"N*4N=&gt;C?$!/_OVBPTXC(ZP`L`VO=Y\PT\(1Q=!`/+@T\X&gt;C@4_[(TXW&gt;W&lt;W_\=XNXB&gt;P^[=&lt;`&lt;^'VJ&lt;_P7)?/T@YNSWJ6_@&lt;]\X`+V*P^Y-^ONPC^0X_GW&amp;[`D\[O`?8Q`@?D&gt;[`.'G%WP@YW]7H&lt;5:&lt;H\DD_Y[=6A_@8[\K#?F``E2&gt;QNJ/.2#?\%&gt;?\?A\&gt;M[@H[/(\`LYY@U\Y&lt;5&amp;&amp;)SS3#&gt;N(*X&lt;;)H?K)H?K)H?K!(?K!(?K!(OK-\OK-\OK-\OK%&lt;OK%&lt;OK%&lt;?GHI1B?[U.G6:0&amp;EI;2I5C!:$)K3,O&amp;*?"+?B)?@3HA3HI1HY5FY'++%*_&amp;*?"+?B)&gt;J3HA3HI1HY5FY+&amp;6)MD2U?")?SCPA#8A#HI!HY'&amp;*"4Q"1,"95$AI!I9#-`A3]!1]!1^@&amp;@!%0!&amp;0Q"0Q9#PA#8A#HI!HY'&amp;+W:5I.&amp;V$BY=S=HA=(I@(Y8&amp;Y+#W(R_&amp;R?"Q?BY@FZ0!Y0![%M["4(!1ZEZQ"TA_(R_(B)I@(Y8&amp;Y("[("[M])3]\U^&amp;U$2U?A]@A-8A-(I/(%D*Y$"[$R_!R?#ALA]@A-8A-(I/(J74Q'$Q'DQ&amp;C,-LS-II:%YV"BG$Q]#GHR=J4CE*CJ&gt;7`ZM."64W!KA&gt;,^=#I(A46"[T[Y&amp;1@C/J'KW[A[M;IXL$KD;A#KC[M7F"VI&amp;&lt;U3X*/TMA*?57/S1PSD$TJJP\BA;P63MPF5P0Z8,0:4*0*2&amp;&gt;86RK0R\KYO.$:W:F/4EZ_P;V/;1`2&lt;.^,VXQHHZ(0[`6RPOD'&lt;-&gt;_;&gt;@./UW&lt;DT_GT9@PUW&lt;W&gt;5"?.O^@4EN_`(S*.W$-I*G?$JJ0L\F?$MK]N[`5P0F7VGM?XEP`Q\N24^4OP/&lt;:IZ]U_4RF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Edwards-AGC.constructor.vi" Type="VI" URL="../Edwards-AGC.constructor.vi"/>
		<Item Name="Edwards-AGC.destructor.vi" Type="VI" URL="../Edwards-AGC.destructor.vi"/>
		<Item Name="Edwards-AGC.initialize.vi" Type="VI" URL="../Edwards-AGC.initialize.vi"/>
		<Item Name="Edwards-AGC.read pressure.vi" Type="VI" URL="../Edwards-AGC.read pressure.vi"/>
		<Item Name="Edwards-AGC.set gauge onoff.vi" Type="VI" URL="../Edwards-AGC.set gauge onoff.vi"/>
		<Item Name="Edwards-AGC.get data to modify.vi" Type="VI" URL="../Edwards-AGC.get data to modify.vi"/>
		<Item Name="Edwards-AGC.set modified data.vi" Type="VI" URL="../Edwards-AGC.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Edwards-AGC.get i attribute.vi" Type="VI" URL="../Edwards-AGC.get i attribute.vi"/>
		<Item Name="Edwards-AGC.set i attribute.vi" Type="VI" URL="../Edwards-AGC.set i attribute.vi"/>
		<Item Name="Edwards-AGC.ProcCases.vi" Type="VI" URL="../Edwards-AGC.ProcCases.vi"/>
		<Item Name="Edwards-AGC.ProcPeriodic.vi" Type="VI" URL="../Edwards-AGC.ProcPeriodic.vi"/>
		<Item Name="Edwards-AGC.get names.vi" Type="VI" URL="../Edwards-AGC.get names.vi"/>
		<Item Name="Edwards-AGC.get address.vi" Type="VI" URL="../Edwards-AGC.get address.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Edwards-AGC.i attribute.ctl" Type="VI" URL="../Edwards-AGC.i attribute.ctl"/>
		<Item Name="Edwards-AGC.i attribute.vi" Type="VI" URL="../Edwards-AGC.i attribute.vi"/>
		<Item Name="Edwards-AGC.ProcEvents.vi" Type="VI" URL="../Edwards-AGC.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="Edwards-AGC.contents.vi" Type="VI" URL="../Edwards-AGC.contents.vi"/>
</Library>
