﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class supports the C-663 Mercury Step stop motor controller from Physik Instrumente (PI).  Up to 16 controllers can be connected to one interface by a daisy chain. An object of this class talks to all controllers connected to the same interface. Since a controller controls exactly one motor, this class uses the word "axis" as a synonym for "controller".

author: Dietrich Beck, GSI
maintainer: Dietrich Beck, GSI; d.beck@gsi.de

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2004

INFO2SF
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*9!!!*Q(C=\:1R=BMR$%7`-SH3_A9:&gt;8'[I-E"&gt;!4L#LC#7J&gt;K5[B!E1.Y5[7.LI!L[!K[QO9N&amp;ML)-ZH6W%FG8)15N/1(#&lt;QFO:3KP*0?;LR7\B@,Z"_H7L`K&gt;\??=W/=G\03T`(#X`-8`?@GL_(&gt;?4J`ZDCXW\`-@[VIM@TX0].`7_60`5`WZ!8_F_@`48H^`OP(__([2`"PCX24J&amp;BCA4FG^8:DIC&gt;[IC&gt;[IC&gt;[I!&gt;[I!&gt;[I!?[ITO[ITO[ITO[I2O[I2O[I2N[683B#VXIL%K30%G5"%U#*)."5@+8]#1]#5`#1V=*4]+4]#1]#1^$F0!E0!F0QJ0Q-%U*4]+4]#1]#1_BCC3LIM/4]""?!5`!%`!%0!%0+28Q"!""MC"Q%!1-"=[A%@!%0!%0415]!5`!%`!%0,A6]!1]!5`!%`!QJ69FCG;K[0!12A[0Q_0Q/$Q/$[(F]$A]$I`$Y`#14A[0Q_.!/!G&gt;Y#$)G?1-=$I/D]0$1Q[0Q_0Q/$Q/$[\;);_6G7CGCA[0Q70Q'$Q'D]&amp;$#"E]"I`"9`!90)36Q70Q'$Q'D]&amp;$+BE]"I`"9Y!935EP)ZARU2BE#!90PTIN6LM527*6_^/=$[L[!+I0FPL!K!_#?I06'[@?%06#KR&gt;1P4$K&amp;V;`C"J1H6A&gt;5$V1*`[0W!%&lt;M"WWR4&lt;9'FNB&gt;?`^Z9'HUUH(YV'(QU($-'CXWWG\X7KTW7C^8GOV7OHSRHV0H=P._6Z[I,X``OX$V\O\R`W!@@JYOX`]]783T`\HW??WZ8(TP@1;\E;^U8BRT&lt;.'0Q(GHZC_!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PIMercuryStep.constructor.vi" Type="VI" URL="../PIMercuryStep.constructor.vi"/>
		<Item Name="PIMercuryStep.destructor.vi" Type="VI" URL="../PIMercuryStep.destructor.vi"/>
		<Item Name="PIMercuryStep.get data to modify.vi" Type="VI" URL="../PIMercuryStep.get data to modify.vi"/>
		<Item Name="PIMercuryStep.set modified data.vi" Type="VI" URL="../PIMercuryStep.set modified data.vi"/>
		<Item Name="PIMercuryStep.close.vi" Type="VI" URL="../PIMercuryStep.close.vi"/>
		<Item Name="PIMercuryStep.emergency off.vi" Type="VI" URL="../PIMercuryStep.emergency off.vi"/>
		<Item Name="PIMercuryStep.find axis reference.vi" Type="VI" URL="../PIMercuryStep.find axis reference.vi"/>
		<Item Name="PIMercuryStep.get axis acceleration.vi" Type="VI" URL="../PIMercuryStep.get axis acceleration.vi"/>
		<Item Name="PIMercuryStep.get axis onOff.vi" Type="VI" URL="../PIMercuryStep.get axis onOff.vi"/>
		<Item Name="PIMercuryStep.get axis position.vi" Type="VI" URL="../PIMercuryStep.get axis position.vi"/>
		<Item Name="PIMercuryStep.get axis status.vi" Type="VI" URL="../PIMercuryStep.get axis status.vi"/>
		<Item Name="PIMercuryStep.get axis stop velocity.vi" Type="VI" URL="../PIMercuryStep.get axis stop velocity.vi"/>
		<Item Name="PIMercuryStep.halt axis.vi" Type="VI" URL="../PIMercuryStep.halt axis.vi"/>
		<Item Name="PIMercuryStep.ID Query.vi" Type="VI" URL="../PIMercuryStep.ID Query.vi"/>
		<Item Name="PIMercuryStep.initialize.vi" Type="VI" URL="../PIMercuryStep.initialize.vi"/>
		<Item Name="PIMercuryStep.move axis absolute.vi" Type="VI" URL="../PIMercuryStep.move axis absolute.vi"/>
		<Item Name="PIMercuryStep.move axis relative.vi" Type="VI" URL="../PIMercuryStep.move axis relative.vi"/>
		<Item Name="PIMercuryStep.set axis acceleration.vi" Type="VI" URL="../PIMercuryStep.set axis acceleration.vi"/>
		<Item Name="PIMercuryStep.set axis onOff.vi" Type="VI" URL="../PIMercuryStep.set axis onOff.vi"/>
		<Item Name="PIMercuryStep.set axis position.vi" Type="VI" URL="../PIMercuryStep.set axis position.vi"/>
		<Item Name="PIMercuryStep.set axis stop velocity.vi" Type="VI" URL="../PIMercuryStep.set axis stop velocity.vi"/>
		<Item Name="PIMercuryStep.reset.vi" Type="VI" URL="../PIMercuryStep.reset.vi"/>
		<Item Name="PIMercuryStep.reset axis.vi" Type="VI" URL="../PIMercuryStep.reset axis.vi"/>
		<Item Name="PIMercuryStep.stop axis.vi" Type="VI" URL="../PIMercuryStep.stop axis.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PIMercuryStep.get i attribute.vi" Type="VI" URL="../PIMercuryStep.get i attribute.vi"/>
		<Item Name="PIMercuryStep.set i attribute.vi" Type="VI" URL="../PIMercuryStep.set i attribute.vi"/>
		<Item Name="PIMercuryStep.ProcCases.vi" Type="VI" URL="../PIMercuryStep.ProcCases.vi"/>
		<Item Name="PIMercuryStep.ProcPeriodic.vi" Type="VI" URL="../PIMercuryStep.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PIMercuryStep.i attribute.ctl" Type="VI" URL="../PIMercuryStep.i attribute.ctl"/>
		<Item Name="PIMercuryStep.i attribute.vi" Type="VI" URL="../PIMercuryStep.i attribute.vi"/>
		<Item Name="PIMercuryStep.ProcEvents.vi" Type="VI" URL="../PIMercuryStep.ProcEvents.vi"/>
		<Item Name="PIMercuryStep.get addresses.vi" Type="VI" URL="../PIMercuryStep.get addresses.vi"/>
		<Item Name="PIMercuryStep.get axis VISA resource.vi" Type="VI" URL="../PIMercuryStep.get axis VISA resource.vi"/>
		<Item Name="PIMercuryStep.get interface.vi" Type="VI" URL="../PIMercuryStep.get interface.vi"/>
		<Item Name="PIMercuryStep.get parameters.vi" Type="VI" URL="../PIMercuryStep.get parameters.vi"/>
		<Item Name="PIMercuryStep.convert status.vi" Type="VI" URL="../PIMercuryStep.convert status.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
	</Item>
	<Item Name="PIMercuryStep.contents.vi" Type="VI" URL="../PIMercuryStep.contents.vi"/>
	<Item Name="PIMercuryStep_db.ini" Type="Document" URL="../PIMercuryStep_db.ini"/>
	<Item Name="PIMercuryStep_mapping.ini" Type="Document" URL="../PIMercuryStep_mapping.ini"/>
</Library>
