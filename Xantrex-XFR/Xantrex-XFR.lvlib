﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Programme_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Programme\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Programme_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">C:\Programme\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">Class for a XANTREX XFR power supply. 

More info to be added

author?
maintainer?

license?

</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)`!!!*Q(C=\:3R&lt;3-R%%7`$A[=#DDA5G.&lt;G.4B!KZ!,5Q,3O6-,5TO3"U=Z"+G"&lt;7A&amp;OCXMZ40.G!*-'TY!J-;C@NH/0/7J#B6OZ;OV#[VPW@&lt;S&gt;`++P\6J/&gt;BGU,;P^#X`FFO\&lt;R`FD[1PZV#XMX`XPN&gt;;DL&lt;@PQ``O`V8T[_G]O(`'O&lt;N#B3,,(!(,.[OZ&lt;IC:\IC:\IC2\IA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\IBFY&gt;8?B#&amp;TKLEB20#C6*EQ2*-#B+PB+?B#@B38BY6-+4]#1]#5`#1YA3HI1HY5FY%B[G+?&amp;*?"+?B#@B)672:(6U?")?UCPA#8A#HI!HY+'E!JY!)#A7*![3A+(!'1Q#HI!HY''IA#@A#8A#HI!(NQ+?A#@A#8A#(K&lt;5KE424"U&gt;(N,)Y8&amp;Y("[(R_%BN2Q?B]@B=8A=(ML*Y8&amp;Y(!CHI*-="$G4H!$HQ?&amp;R?0C2Q_0Q/$Q/D]/$KX&lt;);W5GGKGDQW0Q'$Q'D]&amp;D]*"#"I`"9`!90!90;78Q'$Q'D]&amp;D]&amp;"+"I`"9`!9)%:2SMN):EQUAAT"Y/&amp;4J]6KF[*)L(L`;]Y(6@U!KB]M^1/D@B$5.VB^Y^1X2(WBV2&gt;1@7(58VD^2&gt;1"V1OL*V10V*(P!\&lt;(&gt;NA77W-L&lt;-1'&lt;$F.`?4!Y`'IQ_'A`8[PX7[H\8;L^8KNV7KF=2QV$)/7S_8T&lt;86$H^PC&gt;#^N'.``?6RM(I&lt;&amp;ZAY&lt;&lt;WN]`ZMRPF0-:^N],`U0&gt;[._K&lt;WYZFGD*VG#U^Y!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Xantrex-XFR.constructor.vi" Type="VI" URL="../Xantrex-XFR.constructor.vi"/>
		<Item Name="Xantrex-XFR.destructor.vi" Type="VI" URL="../Xantrex-XFR.destructor.vi"/>
		<Item Name="Xantrex-XFR.initialize.vi" Type="VI" URL="../Xantrex-XFR.initialize.vi"/>
		<Item Name="Xantrex-XFR.close.vi" Type="VI" URL="../Xantrex-XFR.close.vi"/>
		<Item Name="Xantrex-XFR.emergency off.vi" Type="VI" URL="../Xantrex-XFR.emergency off.vi"/>
		<Item Name="Xantrex-XFR.ID query.vi" Type="VI" URL="../Xantrex-XFR.ID query.vi"/>
		<Item Name="Xantrex-XFR.reset.vi" Type="VI" URL="../Xantrex-XFR.reset.vi"/>
		<Item Name="Xantrex-XFR.get channel current.vi" Type="VI" URL="../Xantrex-XFR.get channel current.vi"/>
		<Item Name="Xantrex-XFR.get channel voltage.vi" Type="VI" URL="../Xantrex-XFR.get channel voltage.vi"/>
		<Item Name="Xantrex-XFR.set channel current.vi" Type="VI" URL="../Xantrex-XFR.set channel current.vi"/>
		<Item Name="Xantrex-XFR.set channel on off.vi" Type="VI" URL="../Xantrex-XFR.set channel on off.vi"/>
		<Item Name="Xantrex-XFR.get data to modify.vi" Type="VI" URL="../Xantrex-XFR.get data to modify.vi"/>
		<Item Name="Xantrex-XFR.set modified data.vi" Type="VI" URL="../Xantrex-XFR.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Xantrex-XFR.check range current.vi" Type="VI" URL="../Xantrex-XFR.check range current.vi"/>
		<Item Name="Xantrex-XFR.get address.vi" Type="VI" URL="../Xantrex-XFR.get address.vi"/>
		<Item Name="Xantrex-XFR.get limits.vi" Type="VI" URL="../Xantrex-XFR.get limits.vi"/>
		<Item Name="Xantrex-XFR.get i attribute.vi" Type="VI" URL="../Xantrex-XFR.get i attribute.vi"/>
		<Item Name="Xantrex-XFR.set i attribute.vi" Type="VI" URL="../Xantrex-XFR.set i attribute.vi"/>
		<Item Name="Xantrex-XFR.ProcCases.vi" Type="VI" URL="../Xantrex-XFR.ProcCases.vi"/>
		<Item Name="Xantrex-XFR.ProcPeriodic.vi" Type="VI" URL="../Xantrex-XFR.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Xantrex-XFR.i attribute.ctl" Type="VI" URL="../Xantrex-XFR.i attribute.ctl"/>
		<Item Name="Xantrex-XFR.i attribute.vi" Type="VI" URL="../Xantrex-XFR.i attribute.vi"/>
		<Item Name="Xantrex-XFR.ProcEvents.vi" Type="VI" URL="../Xantrex-XFR.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="Xantrex-XFR.contents.vi" Type="VI" URL="../Xantrex-XFR.contents.vi"/>
</Library>
