﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class is a quick hack for the AM300 from Rohde &amp; Schwarz. It is based on an instrument driver from the R&amp;S web-site. Only basic functionality is supported.

maintainer: Dietrich Beck, GSI
author: Dietrich Beck, GSI

License Agreement for this software:

Copyright (C) 2009  Dietrich Beck
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;.

For all questions and ideas contact: d.beck@gsi.de
Last update: 07-Sep-2009</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*J!!!*Q(C=\:1R&lt;BMR%%7`!R=*I%;&amp;_E"8G#P)2VD!+&gt;T/&amp;&gt;3[F)]Q6VB8BEP6[?9+OI,K\4:PO&gt;T)%A)NANC!CZ!;C@NH/0/7J#C6^F7[64`8&amp;F=&lt;`C&amp;E$"M'CV&amp;96/P\52L&gt;A[_@2F0M[?@U=/9`J:N3`7H_W:SX^7@YZZKONP`_^`:XV`V&gt;VVXT&gt;^VZQ)7`[SY#`N)`FX_7&lt;`&lt;^,NK`__?0^_0]H_"DGX243,(%!H0-SNPVC:\IC:\IC:\IA2\IA2\IA2\IDO\IDO\IDO\IBG\IBG\IBG\IJ;-,8?B#:V73YEGB*'G3)!E'2=F8QJ0Q*$Q*$Y^+?"+?B#@B38A)5=+4]#1]#5`#QT1F0!F0QJ0Q*$SE+C2:/DI]#1`J&amp;@!%0!&amp;0Q"0Q5&amp;)"4Q!1&amp;!M3"UH!5/!-"A&amp;0Q"0Q-&amp;4!%`!%0!&amp;0Q).&lt;!5`!%`!%0!%05]KK2+%:/DI]J*($Y`!Y0![0QU.K/4Q/D]0D]$A]F*0$Y`!Y%%Z"*TE)=C9Z!=[$Q_0Q]#/(R_&amp;R?"Q?BQ&gt;8W3%P+T01$"U&gt;(I0(Y$&amp;Y$"[$BR1S?!Q?A]@A-8B)+Y0(Y$&amp;Y$"[$BV)S?!Q?A]=!-9J38E9S9[)2:!A'$Z^S7KTM5B13+\X_.=?$KHI!61_7[I&amp;201CK'[S[=;I&lt;ILL1KAOIOD#K,[T[)KK!KI66%[I'[MDX!&gt;ND,&lt;&lt;$NFC$&lt;&lt;!VNBSGPH0A]8D5Y8$1@L^8W\&lt;;\8&lt;;&lt;L&gt;KGE;&lt;T5&lt;L^6L,Z@,X&lt;@7&gt;0L;&lt;[6Z[:0T[\@[J8@VMHB^_.#_LO_:Z&gt;@]U[*0`)WS]FT\$X;AP[N^=][T2,RWFB"!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">0.1.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Item Name="RS_AM300.constructor.vi" Type="VI" URL="../RS_AM300.constructor.vi"/>
		<Item Name="RS_AM300.destructor.vi" Type="VI" URL="../RS_AM300.destructor.vi"/>
		<Item Name="RS_AM300.get data to modify.vi" Type="VI" URL="../RS_AM300.get data to modify.vi"/>
		<Item Name="RS_AM300.reset.vi" Type="VI" URL="../RS_AM300.reset.vi"/>
		<Item Name="RS_AM300.set amplitude.vi" Type="VI" URL="../RS_AM300.set amplitude.vi"/>
		<Item Name="RS_AM300.set DC offset.vi" Type="VI" URL="../RS_AM300.set DC offset.vi"/>
		<Item Name="RS_AM300.set frequency.vi" Type="VI" URL="../RS_AM300.set frequency.vi"/>
		<Item Name="RS_AM300.set external reference.vi" Type="VI" URL="../RS_AM300.set external reference.vi"/>
		<Item Name="RS_AM300.set modified data.vi" Type="VI" URL="../RS_AM300.set modified data.vi"/>
		<Item Name="RS_AM300.set phase.vi" Type="VI" URL="../RS_AM300.set phase.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="RS_AM300.get i attribute.vi" Type="VI" URL="../RS_AM300.get i attribute.vi"/>
		<Item Name="RS_AM300.ProcCases.vi" Type="VI" URL="../RS_AM300.ProcCases.vi"/>
		<Item Name="RS_AM300.ProcPeriodic.vi" Type="VI" URL="../RS_AM300.ProcPeriodic.vi"/>
		<Item Name="RS_AM300.set i attribute.vi" Type="VI" URL="../RS_AM300.set i attribute.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="RS_AM300.i attribute.ctl" Type="VI" URL="../RS_AM300.i attribute.ctl"/>
		<Item Name="RS_AM300.get interface.vi" Type="VI" URL="../RS_AM300.get interface.vi"/>
		<Item Name="RS_AM300.i attribute.vi" Type="VI" URL="../RS_AM300.i attribute.vi"/>
		<Item Name="RS_AM300.ProcEvents.vi" Type="VI" URL="../RS_AM300.ProcEvents.vi"/>
	</Item>
	<Item Name="RS_AM300.contents.vi" Type="VI" URL="../RS_AM300.contents.vi"/>
	<Item Name="RS_AM300_mapping.ini" Type="Document" URL="../RS_AM300_mapping.ini"/>
	<Item Name="RS_AM300_db.ini" Type="Document" URL="../RS_AM300_db.ini"/>
</Library>
