﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Programme_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Programme\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Programme_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">C:\Programme\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">Power Meter 100D from Thorlabs. 
Should be used together with Thorlabs_PowerMeter100D_GUI.
Known bugs: 

- Sometimes the readout of the sensor repeats 0. Maybe this is due to a laser pulse with a too low energy. But this has to be further investigated.

- The saving procedure in the GUI is not optimal. The file is opened and closed two times per shot and each time the data is written completely into the file.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,Y!!!*Q(C=T:3R&lt;B."%)&lt;`1S!:J@)&lt;B,C-O[EIE9+5&amp;UB(0;+E3Y5%$@)L4%?(:1HF!9+A3D&gt;.J,D-#V"%6-C+,)\P.GNER=[Z!9H:G\8^`\OTX_WN4SIRE"[LX26\P&gt;(DF\E0_WX*`PI\`.\[&gt;95?PLV?`]\I]=PG\.[@8;(?Q/_&gt;U`GX;\'Y(&gt;_/\`NL-\@\FR?8?&amp;W`7"YO.`U,7FP[R:CY\V`C8,;F8RQO$T@]F5G`7!YW[[_+UW`VWQL8]7_LPXZ`7`A7[\(&amp;(S]\M@:&lt;`/7]MZ;DZ10__+I42_8;ZL&gt;TQ%&lt;FWP&lt;]C+OZ."JJLIV9D&lt;W;UT:N\4Y`OY`@O^W(^._'V"23-MEAH&lt;2S&gt;WWC*XKC*XKC*XKA"XKA"XKA"\KD/\KD/\KD/\KB'\KB'\KB'XJJ[%)8ON$:F74R:+'E;&amp;)A'1S+EC\B38A3HI3(HUJY%J[%*_&amp;*?"CCB#@B38A3HI3(;5JY%J[%*_&amp;*?#B63,)U&gt;(A3(MILY!FY!J[!*_"B315]!5#Q7&amp;!Y+!+'!D0Y%P!%0!%08R8Q"$Q"4]!4]'!LY!FY!J[!*_"B3NG6+$2&gt;1Y?(-H*Y("[(R_&amp;R?#ANB]@B=8A=(I?(Z?4Q/$Q/B,/A5RQ%/:/=!=Y0B]@BY5-/D]0D]$A]$A^7?5*?&gt;K;D[2I[0!;0Q70Q'$Q'$S6E]"A]"I`"9`"16A;0Q70Q'$Q'$UP*Y$&amp;Y$"Y$R&amp;C5Z755-S9;AQT"Y/%KJ]8+5YJ#9K86P_&lt;&gt;165^A+I(3`8!K"Y%V1?M_O"5(YDK2KNOI/L'K.[Q[IWI!KIOL&amp;J1&gt;;"O[+`*=X*'4MB4]I1])A`)94@V,Q_]O&lt;H2^@7VTM`0.:P..*F-&gt;(J[KJ/4%RU&gt;(?HAY%$$Y@$0WWK@&gt;B@.[LXUDO`E=`)F_92]3A\*:_1,]GUX:D8W7\NI0GL;H0W;.J^`4JP:^Q&amp;ZX(R[.3VZ^P59&lt;]#9140&gt;(T2@XP"Z0#DT0LR7]`Z(7;_Z?S`^$_^'06+\^JJHDXY$?QEG,1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Thorlabs_PowerMeter100D.constructor.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.constructor.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.destructor.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.destructor.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.get data to modify.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.get data to modify.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.set modified data.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.set modified data.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.Initialize.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.Initialize.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.GetValue.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.GetValue.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.Zeroing.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.Zeroing.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Thorlabs_PowerMeter100D.get i attribute.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.get i attribute.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.set i attribute.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.set i attribute.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.ProcCases.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.ProcCases.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.ProcPeriodic.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Thorlabs_PowerMeter100D.i attribute.ctl" Type="VI" URL="../Thorlabs_PowerMeter100D.i attribute.ctl"/>
		<Item Name="Thorlabs_PowerMeter100D.i attribute.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.i attribute.vi"/>
		<Item Name="Thorlabs_PowerMeter100D.ProcEvents.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="Thorlabs_PowerMeter100D.Contents.vi" Type="VI" URL="../Thorlabs_PowerMeter100D.Contents.vi"/>
</Library>
