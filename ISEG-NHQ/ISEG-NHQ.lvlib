﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Programme_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Programme\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Programme_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">C:\Programme\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">Class for a ISEG-NHQ power supply. 

More info to be added

author?
maintainer?

license?</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*+!!!*Q(C=\&gt;3^43N2%)&lt;B&lt;[])E*R=&gt;Q#O!'EC*%+XY";G";?%4PF**C2V#WZB7L"U+\"O"]O\R\0)-AAD!2)"OZSV`:W`:]]?6GL(O83G`N1R?@@Y3(V@(XV&gt;8^?0F^:WUL`2@V)^D[=&lt;M\'K0S*^&gt;0R$XX(^Z_\`V+&amp;XD^`[X`L0V:`?@L?H._HX(F,8J*3E"-5JVO[O4`)E4`)E4`)E$`)A$`)A$`)A&gt;X)H&gt;X)H&gt;X)H.X)D.X)D.X)D&lt;S?ZS%5O=F9FG4S:+"EU'3"J$%8**@%EHM34?0CJR*.Y%E`C34QU5?**0)EH]31?OCHR**\%EXA3$U-V3&lt;;4(%`C98A&amp;HM!4?!*0Y'&amp;+":Y!%%Q7$"Q-!E.":@!F]!3?Q-.8":\!%XA#4_#B7I%H]!3?Q".Y[.*7*:JG/-HR-)Q=D_.R0)\(]4#U()`D=4S/R`%QH2S0YX%1TI4/Y"$E&gt;();/$]=D_0B1Y\(]4A?R_.YK'J0S.P+$*LB*-&gt;D?!S0Y4%]BI=B:(A-D_%R0);(976Y$)`B-4S'B[FE?!S0Y4%ARK2-,W-QI[02S!A-$X^NNVB\3N%EVM\[V^RP6.5'6'UMV9:2&lt;146!V9^/.5$53WU;A&amp;6#[/[9&gt;7.K)#KC65$KBJKRX6,W6$7F"6F36F1ZJ1::4JU`?+'O^V/W_V7G]V'[`6;K^6+S_63C]6#]`F=M^F-U_HUZ7VVQ&lt;E`OP'^&gt;-PXI4T=`/M?H`ZW^^@K\C[PWP?(G``&gt;7(`9^CP+`LXU%^[._K0_Y$80'DU$;V"601!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="ISEG-NHQ.constructor.vi" Type="VI" URL="../ISEG-NHQ.constructor.vi"/>
		<Item Name="ISEG-NHQ.destructor.vi" Type="VI" URL="../ISEG-NHQ.destructor.vi"/>
		<Item Name="ISEG-NHQ.initialize.vi" Type="VI" URL="../ISEG-NHQ.initialize.vi"/>
		<Item Name="ISEG-NHQ.close.vi" Type="VI" URL="../ISEG-NHQ.close.vi"/>
		<Item Name="ISEG-NHQ.reset.vi" Type="VI" URL="../ISEG-NHQ.reset.vi"/>
		<Item Name="ISEG-NHQ.ID query.vi" Type="VI" URL="../ISEG-NHQ.ID query.vi"/>
		<Item Name="ISEG-NHQ.set channel trip current.vi" Type="VI" URL="../ISEG-NHQ.set channel trip current.vi"/>
		<Item Name="ISEG-NHQ.set channel ramp speed.vi" Type="VI" URL="../ISEG-NHQ.set channel ramp speed.vi"/>
		<Item Name="ISEG-NHQ.set channel voltage.vi" Type="VI" URL="../ISEG-NHQ.set channel voltage.vi"/>
		<Item Name="ISEG-NHQ.set channel on off.vi" Type="VI" URL="../ISEG-NHQ.set channel on off.vi"/>
		<Item Name="ISEG-NHQ.get channel voltage.vi" Type="VI" URL="../ISEG-NHQ.get channel voltage.vi"/>
		<Item Name="ISEG-NHQ.get channel current.vi" Type="VI" URL="../ISEG-NHQ.get channel current.vi"/>
		<Item Name="ISEG-NHQ.get channel trip current.vi" Type="VI" URL="../ISEG-NHQ.get channel trip current.vi"/>
		<Item Name="ISEG-NHQ.get channel status.vi" Type="VI" URL="../ISEG-NHQ.get channel status.vi"/>
		<Item Name="ISEG-NHQ.get data to modify.vi" Type="VI" URL="../ISEG-NHQ.get data to modify.vi"/>
		<Item Name="ISEG-NHQ.set modified data.vi" Type="VI" URL="../ISEG-NHQ.set modified data.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="ISEG-NHQ.check range voltage.vi" Type="VI" URL="../ISEG-NHQ.check range voltage.vi"/>
		<Item Name="ISEG-NHQ.get i attribute.vi" Type="VI" URL="../ISEG-NHQ.get i attribute.vi"/>
		<Item Name="ISEG-NHQ.set i attribute.vi" Type="VI" URL="../ISEG-NHQ.set i attribute.vi"/>
		<Item Name="ISEG-NHQ.get address.vi" Type="VI" URL="../ISEG-NHQ.get address.vi"/>
		<Item Name="ISEG-NHQ.get limits.vi" Type="VI" URL="../ISEG-NHQ.get limits.vi"/>
		<Item Name="ISEG-NHQ.ProcCases.vi" Type="VI" URL="../ISEG-NHQ.ProcCases.vi"/>
		<Item Name="ISEG-NHQ.ProcPeriodic.vi" Type="VI" URL="../ISEG-NHQ.ProcPeriodic.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ISEG-NHQ.i attribute.ctl" Type="VI" URL="../ISEG-NHQ.i attribute.ctl"/>
		<Item Name="ISEG-NHQ.i attribute.vi" Type="VI" URL="../ISEG-NHQ.i attribute.vi"/>
		<Item Name="ISEG-NHQ.ProcEvents.vi" Type="VI" URL="../ISEG-NHQ.ProcEvents.vi"/>
	</Item>
	<Item Name="inheritance" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PRIVATE.ProcCases.vi" Type="VI" URL="../inheritance/PRIVATE.ProcCases.vi"/>
		<Item Name="PUBLIC.constructor.vi" Type="VI" URL="../inheritance/PUBLIC.constructor.vi"/>
		<Item Name="PUBLIC.destructor.vi" Type="VI" URL="../inheritance/PUBLIC.destructor.vi"/>
	</Item>
	<Item Name="ISEG-NHQ.contents.vi" Type="VI" URL="../ISEG-NHQ.contents.vi"/>
</Library>
